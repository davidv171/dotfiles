#!/usr/bin/env bash
# this is a simple config for herbstluftwm
hc() {
    herbstclient "$@"
}

hc emit_hook reload
wmname LG3D


# remove all existing keybindings
hc keyunbind --all
hc set default_frame_layout 3
hc spawn bash "$HOME/.config/herbstluftwm/dialogListener"
hc floating dialog on
# keybindings
# if you have a super key you will be much happier with Mod set to Mod4
Mod=Mod4
#Mod=Mod4   # Use the super key as the main modifier
hc keybind $Mod-Control-r spawn redshift -O 3100
hc keybind $Mod-Shift-q quit
hc keybind $Mod-Shift-r reload
hc keybind $Mod-q close_or_remove
hc keybind $Mod-Return spawn kitty
hc keybind $Mod-Shift-Return spawn sh "$HOME/.config/herbstluftwm/newFrameAndTerminal" right kitty
hc keybind $Mod-Tab spawn rofi -show combi
hc keybind XF86MonBrightnessUp spawn sh  "$HOME/.config/herbstluftwm/systemControl" brightnessUp 5
hc keybind XF86MonBrightnessDown spawn sh "$HOME/.config/herbstluftwm/systemControl" brightnessDown 5
hc keybind XF86AudioRaiseVolume spawn sh "$HOME/.config/herbstluftwm/systemControl" volumeUp 5
# Lower Volume
hc keybind XF86AudioLowerVolume spawn sh "$HOME/.config/herbstluftwm/systemControl" volumeDown 5
hc keybind XF86AudioMute spawn amixer -D amixer -D pulse set Master 1+ toggle # Toggle muting
hc keybind $Mod-b spawn firefox
hc keybind $Mod-r spawn nautilus
hc keybind $Mod-z spawn MellowPlayer
hc keybind $Mod-Shift-x spawn ~/.config/i3/0x0 area
hc keybind $Mod-Shift-c spawn ~/.config/i3/0x0 text
hc keybind $Mod-u spawn ~/.config/i3/0x0 recorda
hc keybind $Mod-Shift-t spawn ~/Documents/sourceProjects/Theatron/scripts/simplewatch
hc keybind $Mod-n spawn polybar bspwm || killall polybar
hc keybind $Mod-w spawn stalonetray || killall stalonetray
hc keybind $Mod-g set_layout grid
hc keybind $Mod-a cycle_layout +1 horizontal vertical
hc keybind $Mod-y spawn bash "$HOME/.config/herbstluftwm/dropdown"
hc keybind $Mod-x move scratchpad
hc keybind $Mod-Shift-v spawn pavucontrol
hc keybind $Mod-Control-q spawn bash "$HOME/.config/herbstluftwm/killFrame"
hc keybind $Mod-v spawn bash "$HOME/.config/herbstluftwm/dialogs"
# basic movement
# focusing clients
hc keybind $Mod-Left  focus left
hc keybind $Mod-Down  focus down
hc keybind $Mod-Up    focus up
hc keybind $Mod-Right focus right
hc keybind $Mod-h     focus left
hc keybind $Mod-j     focus down
hc keybind $Mod-k     focus up
hc keybind $Mod-l     focus right

# moving clients
hc keybind $Mod-Shift-Left  shift left
hc keybind $Mod-Shift-Down  shift down
hc keybind $Mod-Shift-Up    shift up
hc keybind $Mod-Shift-Right shift right
hc keybind $Mod-Shift-h     shift left
hc keybind $Mod-Shift-j     shift down
hc keybind $Mod-Shift-k     shift up
hc keybind $Mod-Shift-l     shift right

# splitting frames
# create an empty frame at the specified direction
hc keybind $Mod-cacute       split   bottom  0.5
hc keybind $Mod-zcaron       split   right   0.5
hc keybind $Mod-ccaron	split	left	0.5
hc keybind $Mod-scaron	split	top	0.5
hc keybind $Mod-Shift-cacute spawn sh "$HOME/.config/herbstluftwm/newFrameAndTerminal" bottom kitty
hc keybind $Mod-Shift-zcaron spawn sh "$HOME/.config/herbstluftwm/newFrameAndTerminal" right kitty
hc keybind $Mod-Shift-ccaron spawn sh "$HOME/.config/herbstluftwm/newFrameAndTerminal" left kitty
hc keybind $Mod-Shift-scaron spawn sh "$HOME/.config/herbstluftwm/newFrameAndTerminal" top kitty
hc keybind $Mod-Control-scaron split	top	0.8
hc keybind $Mod-Control-cacute split 	bottom	0.8
hc keybind $Mod-Control-zcaron split	right	0.8
hc keybind $Mod-Control-ccaron split	left	0.8
# let the current frame explode into subframes
hc keybind $Mod-Control-space split explode

# resizing frames
resizestep=0.05
hc keybind $Mod-Control-h       resize left +$resizestep
hc keybind $Mod-Control-j       resize down +$resizestep
hc keybind $Mod-Control-k       resize up +$resizestep
hc keybind $Mod-Control-l       resize right +$resizestep
hc keybind $Mod-Control-Left    resize left +$resizestep
hc keybind $Mod-Control-Down    resize down +$resizestep
hc keybind $Mod-Control-Up      resize up +$resizestep
hc keybind $Mod-Control-Right   resize right +$resizestep

# tags
tag_names=( {1..9} )
tag_keys=( {1..9} 0 )

hc rename default "${tag_names[0]}" || true
for i in ${!tag_names[@]} ; do
    hc add "${tag_names[$i]}"
    key="${tag_keys[$i]}"
    if ! [ -z "$key" ] ; then
        hc keybind "$Mod-$key" use_index "$i"
        hc keybind "$Mod-Shift-$key" move_index "$i"
    fi
done
hc add 0
hc add 11
#Our throwaway workspace
hc add 12
hc add 13
hc keybind $Mod-0 use 0
hc keybind cedilla use 11
hc keybind $Mod-s use 6
hc keybind $Mod-Shift-s move 6
hc keybind $Mod-d use 7
hc keybind $Mod-Shift-d move 7
hc keybind $Mod-e move 12
hc keybind $Mod-greater use 13
hc keybind $Mod-Shift-greater move 13
# cycle through tags
hc keybind $Mod-period use_index +1 --skip-visible
hc keybind $Mod-comma  use_index -1 --skip-visible

# layouting
hc keybind $Mod-control-r remove
hc keybind $Mod-f floating toggle
hc keybind $Mod-Shift-f fullscreen toggle
hc keybind $Mod-p pseudotile toggle
# The following cycles through the available layouts within a frame, but skips
# layouts, if the layout change wouldn't affect the actual window positions.
# I.e. if there are two windows within a frame, the grid layout is skipped.
hc keybind $Mod-space                                                           \
            or , and . compare tags.focus.curframe_wcount = 2                   \
                     . cycle_layout +1 vertical horizontal max vertical grid    \
               , cycle_layout +1

# mouse
hc mouseunbind --all
hc mousebind $Mod-Button1 move
hc mousebind $Mod-Button2 zoom
hc mousebind $Mod-Button3 resize

# focus
hc keybind $Mod-BackSpace   cycle_monitor
hc keybind $Mod-c cycle
hc keybind $Mod-i jumpto urgent

# theme
hc attr theme.tiling.reset 1
hc attr theme.floating.reset 1
hc set frame_border_active_color '#4d4d4c'
hc set frame_border_normal_color '#f3f3f3'
hc set frame_bg_normal_color '#f3f3f3'
hc set frame_bg_active_color '#4d4d4c'
hc set frame_border_width 3
hc set always_show_frame 0
hc set frame_bg_transparent 1
hc set frame_transparent_width 1
hc set frame_gap 10
hc set update_dragged_clients 1
hc attr theme.active.color '#4d4d4c'
hc attr theme.normal.color '#f3f3f3'
hc attr theme.urgent.color red
hc attr theme.inner_width 0
hc attr theme.inner_color black
hc attr theme.border_width 0
hc attr theme.floating.border_width 2
hc attr theme.floating.outer_width 2
hc attr theme.floating.outer_color '#f3f3f3'
hc attr theme.active.inner_color '#f3f3f3'
hc attr theme.active.outer_color '#f3f3f3'
hc attr theme.background_color '#f3f3f3'

hc set window_gap 10
hc set frame_padding 0
hc set smart_window_surroundings 1
hc set smart_frame_surroundings 1
hc set mouse_recenter_gap 1

# rules
hc unrule -F
#hc rule class=XTerm tag=3 # move all kittys to tag 3
#hc rule focus=off # normally do not focus new clients
# give focus to most common terminals
#hc rule class~'(.*[Rr]xvt.*|.*[Tt]erm|Konsole)' focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' manage=on focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK)' manage=off focus=on
hc rule class="Firefox" windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' manage=off focus=on tag=dialog hook=dialog
hc rule class="jetbrains-idea" manage=on focus=on tag=3

hc set tree_style '╾│ ├└╼─┐'
# CUSTOM RULES ________________
hc rule focus=on # normally focus new clients
hc rule class="mpv" tag=9
hc rule class="discord" tag=4
hc rule class="scratch" tag=scratchpad

# STUFF TO DO
hc focus_crosses_monitor_boundaries 1
hc set smart_frame_surroundings 1
hc set smart_window_surroundings 1
hc focus_follows_mouse 1
hc detect_monitors
# unlock, just to be sure
hc unlock

# do multi monitor setup here, e.g.:
# hc set_monitors 1280x1024+0+0 1280x1024+1280+0
# or simply:
# hc detect_monitors
# find the panel
# add an external panel
{
    pids=( )
    # reserve some space for the panel on monitor 0
    hc pad 0 31
    # start the panel itself and remember its pid
    polybar bspwm &
    pids+=( $! )
    # or start another panel:
    # mypanel &
    # pids+=( $! )
    # wait until the panels should be stopped
    herbstclient -w '(quit_panel|reload)'
    # stopp all started panels
    kill ${pids[@]}
} &
# Replace the default section for tags in your autostart by the following:
# This is the default tag section of the autostart, with a single change:
# In the use_index keybinding, check the presence of the my_monitor attribute,
# before focusing the desired tag.

hc rename default "${tag_names[0]}" || true
for i in ${!tag_names[@]} ; do
    hc add "${tag_names[$i]}"
    key="${tag_keys[$i]}"
    if ! [ -z "$key" ] ; then
        # first check if the tag is locked to some monitor.
        # if so, first focus the monitor
        hc keybind "$Mod-$key" \
            chain , silent substitute M tags."$i".my_monitor \
                        focus_monitor M \
                  , use_index "$i"
        hc keybind "$Mod-Shift-$key" move_index "$i"
    fi
done

# Add a keybinding for locking the current tag to the monitor it is displayed
# on. This is done by safing the current monitor index in the my_monitor
# attribute of the focused tag. If the monitor has a (nonempty) name, use the
# monitor name instead of its index.
herbstclient keybind $Mod-t chain \
    , new_attr string tags.focus.my_monitor \
    , substitute M monitors.focus.index set_attr tags.focus.my_monitor M \
    , try and \
        . compare monitors.focus.name != "" \
        . substitute M monitors.focus.name \
                set_attr tags.focus.my_monitor M

# Add a keybinding for removing the lock
herbstclient keybind $Mod-Shift-n \
    remove_attr tags.focus.my_monitor

# Statically define which tag should be send to which monitor
lock_tag_to_monitor() {
    herbstclient chain \
        , new_attr string tags.by-name."$1".my_monitor \
        , set_attr tags.by-name."$1".my_monitor "$2"
}
# Already lock some of the tags to a monitor, for example:
# lock the second tag to the monitor with index 0
# Stream tag
# Throwaway workspace
lock_tag_to_monitor 2 1
lock_tag_to_monitor 1 0
lock_tag_to_monitor 3 0
lock_tag_to_monitor 4 0
lock_tag_to_monitor 5 0
lock_tag_to_monitor 6 0
lock_tag_to_monitor 7 1
lock_tag_to_monitor 8 0
lock_tag_to_monitor 9 2
lock_tag_to_monitor 0 0
lock_tag_to_monitor 11 0
lock_tag_to_monitor 12 0
lock_tag_to_monitor 13 0
# AUTO START ----------------------------
hc set focus_follows_mouse 1
hc spawn discord &
hc spawn nitrogen --restore &
hc spawn dunst &
hc spawn ~/Documents/dev/py/Threatron/scripts/pollingservice 60
hc spawn compton --inactive-dim 0.4 &
hc spawn xdo lower -N Polybar &
hc spawn xinput --set-prop 9 'libinput Accel Speed' 1 &
hc spawn nm-applet &
#-----------------------------------
